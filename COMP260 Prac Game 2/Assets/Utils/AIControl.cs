﻿using UnityEngine;
using System.Collections;

public class AIControl : MonoBehaviour {

    public AudioClip wallCollideClip;
    public AudioClip paddleCollideClip;
    public LayerMask paddleLayer;

    private Rigidbody rb;
    Transform target;

    void Start()
    {
        target = GameObject.Find("Puck").transform;                     //Puck Transform (Need to convert to Vector3)
        rb = GetComponent<Rigidbody>();                                 //AIPlayer Rigidbody
    }

    void OnCollisionEnter(Collision collision)
    {
        // check what we have hit
        if (paddleLayer.Contains(collision.gameObject))
        {
            // hit the paddle
            GetComponent<AudioSource>().PlayOneShot(paddleCollideClip);
        }
        else {
            // hit something else
            GetComponent<AudioSource>().PlayOneShot(wallCollideClip);
        }
    }

    public float force = 10f;

    void FixedUpdate()
    {
        Vector3 puckPos = target.position;                             //Converts target(puck) to Vector3
        Vector3 dir = puckPos - rb.position;

        rb.AddForce(dir.normalized * force);
    }
}
