﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class PuckControl : MonoBehaviour
{
    public AudioClip wallCollideClip;
    public AudioClip paddleCollideClip;
    public LayerMask paddleLayer;

    public Transform startingPos;
    private Rigidbody rb;
    private AudioSource audio;

    void Start()
    {
        audio = GetComponent<AudioSource>();
        rb = GetComponent<Rigidbody>();
        ResetPosition();
    }

    void OnCollisionEnter(Collision collision)
    {
        // check what we have hit
        if (paddleLayer.Contains(collision.gameObject))
        {
            // hit the paddle
            GetComponent<AudioSource>().PlayOneShot(paddleCollideClip);
        }
        else {
            // hit something else
            GetComponent<AudioSource>().PlayOneShot(wallCollideClip);
        }
    }

    public void ResetPosition()
    {
        rb.MovePosition(startingPos.position);
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
    }
}